import 'package:intl/intl.dart';

class Util {
  static String formatCurrency(double value, {bool symbol = true}){
    NumberFormat moneyFormat = NumberFormat.currency(
      locale: "pt-BR",
      customPattern: "R\$ #,##0.0#",
    );

    return moneyFormat.format(value);
  }
}