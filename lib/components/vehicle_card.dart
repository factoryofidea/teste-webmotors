import 'package:flutter/material.dart';
import 'package:testewebmotors/models/vehicle.dart';
import 'package:testewebmotors/screens/vehicle_details/vehicle_details_screen.dart';
import 'package:testewebmotors/util.dart';

class VehicleCard extends StatelessWidget {
  Vehicle vehicle;

  VehicleCard({Key? key, required this.vehicle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: ListTile(
        leading: Image.network(
          vehicle.image!,
          errorBuilder:
              (BuildContext context, Object exception, StackTrace? stackTrace) {
            return const FlutterLogo(size: 35);
          },
        ),
        title: Text(vehicle.make! + " " + vehicle.model!),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text("Ano: " + vehicle.yearModel.toString()),

                const SizedBox(width: 10),

                Text("KM: " + vehicle.km.toString()),
              ],
            ),

            const SizedBox(height: 10),

            Text(
              "Preço: " + Util.formatCurrency(
                  double.parse(vehicle.price!.replaceAll(",", "."))
              ),
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.lightBlueAccent.shade400
              ),
            ),
          ],
        ),
        isThreeLine: true,
        trailing: const Icon(Icons.arrow_forward_ios),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>
                VehicleDetailsScreen(vehicle: vehicle)),
          );
        },
      ),
    );
  }
}
