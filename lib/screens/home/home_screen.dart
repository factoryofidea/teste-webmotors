import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:testewebmotors/components/vehicle_card.dart';
import 'package:testewebmotors/models/vehicle.dart';
import 'package:testewebmotors/services/home_service.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PagingController<int, Vehicle> _pagingController =
  PagingController(firstPageKey: 1);

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener((pageKey) {
      _getVehicles(pageKey);
    });
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  Future<void> _getVehicles(int page) async {
    try {
      final newVehicles = await HomeService().getVehicles(page);
      final isLastPage = newVehicles.length < 10;

      if (isLastPage) {
        _pagingController.appendLastPage(newVehicles);
      } else {
        final nextPage = page + 1;
        _pagingController.appendPage(newVehicles, nextPage);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () {
              _pagingController.refresh();
            },
          )
        ],
      ),
      body: _buildBody(context)
    );
  }

  Widget _buildBody(BuildContext context) {
    return PagedListView<int, Vehicle>(
      pagingController: _pagingController,
      padding: const EdgeInsets.all(10),
      builderDelegate: PagedChildBuilderDelegate<Vehicle>(
          itemBuilder: (context, item, index) => VehicleCard(vehicle: item)
      ),
    );
  }
}
