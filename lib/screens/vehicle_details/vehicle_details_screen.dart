import 'package:flutter/material.dart';
import 'package:testewebmotors/models/vehicle.dart';
import 'package:testewebmotors/util.dart';

class VehicleDetailsScreen extends StatefulWidget {
  Vehicle vehicle;

  VehicleDetailsScreen({Key? key, required this.vehicle}) : super(key: key);

  @override
  _VehicleDetailsScreenState createState() => _VehicleDetailsScreenState();
}

class _VehicleDetailsScreenState extends State<VehicleDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.vehicle.make! + " " + widget.vehicle.model!),
        centerTitle: true,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25),
      child: Column(
        children: [
          Image.network(
            widget.vehicle.image!,
            width: 150,
            height: 150,
            errorBuilder:
                (BuildContext context, Object exception, StackTrace? stackTrace) {
              return const FlutterLogo(size: 150);
            },
          ),

          const SizedBox(height: 50),

          _rowDetails("Versão", widget.vehicle.version!),

          const SizedBox(height: 10),

          _rowDetails("Cor", widget.vehicle.color!),

          const SizedBox(height: 10),

          _rowDetails("Ano modelo", widget.vehicle.yearModel!.toString()),

          const SizedBox(height: 10),

          _rowDetails("Ano fabricação", widget.vehicle.yearFab!.toString()),

          const SizedBox(height: 10),

          _rowDetails("KM", widget.vehicle.km!.toString()),

          const SizedBox(height: 10),

          _rowDetails("Preço", Util.formatCurrency(
              double.parse(widget.vehicle.price!.replaceAll(",", "."))
          )),
        ],
      ),
    );
  }

  Widget _rowDetails(String title, String subTitle) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold
          ),
        ),

        Text(subTitle)
      ],
    );
  }
}
