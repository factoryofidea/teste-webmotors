import 'dart:async';

import 'package:http/http.dart';

class API {
  static String BASE_URL = "https://desafioonline.webmotors.com.br/api/OnlineChallenge/";

  var client = Client();

  Future<Response> get(String url) async {
    Uri uri = Uri.parse(BASE_URL + url);

    try {
      Response response = await client.get(uri).timeout(
          const Duration(seconds: 5)
      ).onError((error, stackTrace) {
        return Future.error("Não foi possível estabelecer conexão");
      });

      if (response.statusCode != 200){
        return Future.error("Não foi possível estabelecer conexão");
      }

      return Future.value(response);
    } finally {
      client.close();
    }
  }
}