import 'dart:convert';

import 'package:http/http.dart';
import 'package:testewebmotors/models/vehicle.dart';

import 'API.dart';

class HomeService {
  Future<List<Vehicle>> getVehicles(int page) async {
    try {
      Response response = await API().get("Vehicles?Page=$page");

      return Future.value(List.of(json.decode(response.body) as List<dynamic>)
          .map((json) => Vehicle.fromJson(json)).toList());
    } catch (error) {
      return Future.error(error);
    }
  }
}