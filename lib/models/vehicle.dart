import 'package:json_annotation/json_annotation.dart';

part 'vehicle.g.dart';

@JsonSerializable()
class Vehicle {
  @JsonKey(name: 'ID')
  int? id;
  @JsonKey(name: 'Make')
  String? make;
  @JsonKey(name: 'Model')
  String? model;
  @JsonKey(name: 'Version')
  String? version;
  @JsonKey(name: 'Image')
  String? image;
  @JsonKey(name: 'KM')
  int? km;
  @JsonKey(name: 'Price')
  String? price;
  @JsonKey(name: 'YearModel')
  int? yearModel;
  @JsonKey(name: 'YearFab')
  int? yearFab;
  @JsonKey(name: 'Color')
  String? color;

  Vehicle({
    this.id,
    this.make,
    this.model,
    this.version,
    this.image,
    this.km,
    this.price,
    this.yearModel,
    this.yearFab,
    this.color
  });

  factory Vehicle.fromJson(Map<String, dynamic> json) => _$VehicleFromJson(json);
}